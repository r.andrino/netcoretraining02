﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OdeToFood.Core;

namespace OdeToFood.Data
{
    public class SqlRestaurantData : IRestaurantData
    {
        private readonly OdeToFoodDBContext db;

        public SqlRestaurantData(OdeToFoodDBContext db)
        {
            this.db = db;
        }
        public Restaurant Add(Restaurant newRestaurant)
        {
            db.Add(newRestaurant);
            return newRestaurant;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }

        public Restaurant Delete(int id)
        {
            var r = GetRestaurant(id);
            if(r != null)
            {
                db.Remove(r);
            }
            return r;
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return db.Restaurant.ToList();
        }

        public Restaurant GetRestaurant(int id)
        {
            return db.Restaurant.Find(id);
        }

        public IEnumerable<Restaurant> GetRestaurantByName(string keyword)
        {
            var query = from r in db.Restaurant
                        where string.IsNullOrEmpty(keyword) || r.Name.ToUpper().Contains(keyword.ToUpper()) || r.Location.ToUpper().Contains(keyword.ToUpper())
                        orderby r.Name
                        select r;
            return query;
        }

        public int GetRestaurantsCount()
        {
            return db.Restaurant.Count();
        }

        public Restaurant Update(Restaurant updatedRestaurant)
        {
            var entity = db.Restaurant.Attach(updatedRestaurant);
            entity.State = EntityState.Modified;
            return updatedRestaurant;
        }
    }
}
