﻿using OdeToFood.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdeToFood.Data
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        readonly List<Restaurant> restaurants;
        public InMemoryRestaurantData()
        {
            restaurants = new List<Restaurant>() 
            {
                new Restaurant(){ Id = 1, Name = "Roy's Pizza", Location = "Cebu", Cuisine = CuisineType.Pinoy },
                new Restaurant(){ Id = 2, Name = "Jan's Pizza", Location = "Cebu", Cuisine = CuisineType.Italian },
                new Restaurant(){ Id = 3, Name = "JR's Pizza", Location = "Cebu", Cuisine = CuisineType.Indian },
            };
        }
        public IEnumerable<Restaurant> GetAll()
        {
            return from r in restaurants
                   orderby r.Name
                   select r;
        }

        public IEnumerable<Restaurant> GetRestaurantByName(string name = null)
        {
            return from r in restaurants
                   where string.IsNullOrEmpty(name) || r.Name.ToUpper().StartsWith(name.ToUpper())
                   orderby r.Name
                   select r;
        }
        public Restaurant GetRestaurant(int id)
        {
            var result = from r in restaurants
                   where r.Id.Equals(id)
                   select r;
            return result.FirstOrDefault();
        }

        public Restaurant Update(Restaurant updatedRestaurant)
        {
            var restaurant = restaurants.SingleOrDefault(x => x.Id == updatedRestaurant.Id);
            if(restaurant != null)
            {
                restaurant.Name = updatedRestaurant.Name;
                restaurant.Location = updatedRestaurant.Location;
                restaurant.Cuisine = updatedRestaurant.Cuisine;
            }

            return restaurant;
        }

        public int Commit()
        {
            return 0;
        }

        public Restaurant Add(Restaurant newRestaurant)
        {
            restaurants.Add(newRestaurant);
            newRestaurant.Id = restaurants.Max(x => x.Id) + 1;
            return newRestaurant;
        }

        public Restaurant Delete(int id)
        {
            var r = restaurants.FirstOrDefault(x=>x.Id == id);
            if(r != null)
            {
                restaurants.Remove(r);
            }
            return r;
        }

        public int GetRestaurantsCount()
        {
            return restaurants.Count;
        }
    }
}
